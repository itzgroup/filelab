 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.filelab;

import java.io.Serializable;

/**
 *
 * @author 66955
 */
public class Player implements Serializable{
    private char symbol;
    private int win;
    private int lose;
    private int draw;

    public Player(char symbol) {
        this.symbol = symbol;
    }


    public char getSymbol() {
        return symbol;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void setSymbol(char symbol) {
        this.symbol = symbol;
    }

    public void Win() {
        this.win++;
    }

    public void Lose() {
        this.lose++;
    }

    public void Draw() {
        this.draw++;
    }
        @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
}
